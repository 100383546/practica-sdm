package com.uc3m.proyecto.models

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface CryptocurrencyDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertCrypto(cryptocurrency: Cryptocurrency)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun updateCrypto(cryptocurrency: Cryptocurrency)

    @Query("SELECT encryptedAmount FROM crypto_table WHERE email = :arg0 AND name = :arg1")
    fun readAmount(arg0: String, arg1: String): String

    @Query("SELECT iv FROM crypto_table WHERE email = :arg0 AND name = :arg1")
    fun readIv(arg0: String, arg1: String): String
}
