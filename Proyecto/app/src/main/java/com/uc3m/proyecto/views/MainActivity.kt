package com.uc3m.proyecto.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.uc3m.proyecto.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
