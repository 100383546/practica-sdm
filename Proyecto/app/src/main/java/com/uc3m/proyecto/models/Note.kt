package com.uc3m.proyecto.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "note_table")
data class Note(
    @PrimaryKey
    val email: String,
    val text: String
)
