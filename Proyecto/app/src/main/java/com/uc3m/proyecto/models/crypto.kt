package com.uc3m.proyecto.models

import com.google.gson.annotations.SerializedName

data class Currency(
    @SerializedName("data")
    var rates: Rates
)

data class Rates(
    @SerializedName("rates")
    var price: Price
)

data class Price(
    @SerializedName("EUR")
    var eur: Float
)
