package com.uc3m.proyecto.views

import android.os.Bundle
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.uc3m.proyecto.R
import com.uc3m.proyecto.databinding.FragmentListBinding
import com.uc3m.proyecto.viewModels.CryptocurrencyViewModel
import javax.crypto.KeyGenerator

class ListFragment : Fragment() {
    private lateinit var binding: FragmentListBinding
    private lateinit var auth: FirebaseAuth
    private lateinit var cryptocurrencyViewModel: CryptocurrencyViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(inflater, container, false)
        val view = binding.root

        // obtener usuario
        auth = FirebaseAuth.getInstance()
        val currentUser = auth.currentUser

        binding.user.text = currentUser?.displayName // mostrar nombre de usuario

        cryptocurrencyViewModel = ViewModelProvider(this).get(CryptocurrencyViewModel::class.java)

        if (!cryptocurrencyViewModel.checkKey()) {
            val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
            val keyGenParameterSpec = KeyGenParameterSpec
                .Builder("MyKeyStore", KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .build()

            keyGenerator.init(keyGenParameterSpec)
            keyGenerator.generateKey()
        }

        val adapter = ListAdapter()
        val recyclerView = binding.recyclerView
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        binding.signOutBtn.setOnClickListener {
            auth.signOut()
            findNavController().navigate(R.id.action_listFragment_to_loginFragment)
        }

        binding.notes.setOnClickListener {
            findNavController().navigate(R.id.action_listFragment_to_notesFragment)
        }

        return view
    }
}
