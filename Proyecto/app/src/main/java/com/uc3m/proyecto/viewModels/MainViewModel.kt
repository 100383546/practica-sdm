package com.uc3m.proyecto.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.uc3m.proyecto.models.Currency
import com.uc3m.proyecto.models.Repository
import kotlinx.coroutines.launch
import retrofit2.Response

class MainViewModel(private val repository: Repository) : ViewModel() {
    val myResponse: MutableLiveData<Response<Currency>> = MutableLiveData()

    fun getBitcoin() {
        viewModelScope.launch {
            val response = repository.getBitcoin()
            myResponse.value = response
        }
    }

    fun getLitecoin() {
        viewModelScope.launch {
            val response = repository.getLitecoin()
            myResponse.value = response
        }
    }

    fun getEthereum() {
        viewModelScope.launch {
            val response = repository.getEthereum()
            myResponse.value = response
        }
    }

    fun getLumens() {
        viewModelScope.launch {
            val response = repository.getLumens()
            myResponse.value = response
        }
    }

    fun getDash() {
        viewModelScope.launch {
            val response = repository.getDash()
            myResponse.value = response
        }
    }
}
