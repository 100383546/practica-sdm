package com.uc3m.proyecto.api

import com.uc3m.proyecto.models.Currency
import retrofit2.Response
import retrofit2.http.GET

interface CryptoAPI {
    @GET("v2/exchange-rates?currency=BTC")
    suspend fun getBitcoin(): Response<Currency>

    @GET("v2/exchange-rates?currency=LTC")
    suspend fun getLitecoin(): Response<Currency>

    @GET("v2/exchange-rates?currency=ETH")
    suspend fun getEthereum(): Response<Currency>

    @GET("v2/exchange-rates?currency=XLM")
    suspend fun getLumens(): Response<Currency>

    @GET("v2/exchange-rates?currency=DASH")
    suspend fun getDash(): Response<Currency>
}
