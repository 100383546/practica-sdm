package com.uc3m.proyecto.models

import com.uc3m.proyecto.api.RetrofitInstance
import retrofit2.Response

class Repository {
    suspend fun getBitcoin(): Response<Currency> {
        return RetrofitInstance.cryptoAPI.getBitcoin()
    }

    suspend fun getLitecoin(): Response<Currency> {
        return RetrofitInstance.cryptoAPI.getLitecoin()
    }

    suspend fun getEthereum(): Response<Currency> {
        return RetrofitInstance.cryptoAPI.getEthereum()
    }

    suspend fun getLumens(): Response<Currency> {
        return RetrofitInstance.cryptoAPI.getLumens()
    }

    suspend fun getDash(): Response<Currency> {
        return RetrofitInstance.cryptoAPI.getDash()
    }
}
