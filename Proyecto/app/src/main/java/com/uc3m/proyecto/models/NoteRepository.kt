package com.uc3m.proyecto.models

class NoteRepository(private val noteDao: NoteDAO) {

    fun readNote(email: String): String {
        return noteDao.readNote(email)
    }

    suspend fun updateNote(note: Note) {
        noteDao.updateNote(note)
    }

    suspend fun insertNote(note: Note) {
        noteDao.insertNote(note)
    }
}
