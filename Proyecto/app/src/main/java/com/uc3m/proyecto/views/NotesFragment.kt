package com.uc3m.proyecto.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.firebase.auth.FirebaseAuth
import com.uc3m.proyecto.databinding.FragmentNotesBinding
import com.uc3m.proyecto.models.Note
import com.uc3m.proyecto.viewModels.NoteViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NotesFragment : Fragment() {
    private lateinit var binding: FragmentNotesBinding
    private lateinit var noteViewModel: NoteViewModel
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotesBinding.inflate(inflater, container, false)
        val view = binding.root

        noteViewModel = ViewModelProvider(this).get(NoteViewModel::class.java)

        lifecycleScope.launch {
            // obtener email del usuario
            auth = FirebaseAuth.getInstance()
            val currentUserEmail = auth.currentUser?.email.toString()

            val note = readNote(currentUserEmail)

            binding.editTextTextMultiLine.setText(note) // mostrar nota
        }

        binding.notes.setOnClickListener {
            val note = binding.editTextTextMultiLine.text.toString()
            if (note.isNotEmpty()) updateDataToDatabase() // guardar nota
            else Toast.makeText(requireContext(), "It can´t be empty", Toast.LENGTH_LONG).show()
        }

        return view
    }

    private fun updateDataToDatabase() {
        // obtener email del usuario
        auth = FirebaseAuth.getInstance()
        val currentUserEmail = auth.currentUser?.email

        val email = currentUserEmail.toString()
        val text = binding.editTextTextMultiLine.text.toString()

        val note = Note(email, text)
        noteViewModel.insertNote(note)
        noteViewModel.updateNote(note)
    }

    private suspend fun readNote(email: String): String {
        val note = withContext(Dispatchers.IO) {
            noteViewModel.readNote(email)
        }
        return note
    }
}
