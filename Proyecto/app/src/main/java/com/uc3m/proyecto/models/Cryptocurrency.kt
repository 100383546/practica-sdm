package com.uc3m.proyecto.models

import androidx.room.Entity

@Entity(tableName = "crypto_table", primaryKeys = [ "email", "name" ])
data class Cryptocurrency(
    val email: String,
    val name: String,
    val iv: String,
    val encryptedAmount: String
)
