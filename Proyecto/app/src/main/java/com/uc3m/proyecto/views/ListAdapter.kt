package com.uc3m.proyecto.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.uc3m.proyecto.databinding.RecyclerViewItemBinding

class ListAdapter : RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var cryptocurrencyList = listOf("Bitcoin", "Litecoin", "Ethereum", "Stellar Lumens", "Dash")

    class MyViewHolder(val binding: RecyclerViewItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val binding = RecyclerViewItemBinding.inflate(
            LayoutInflater.from(parent.context), parent,
            false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = cryptocurrencyList[position]
        with(holder) {
            binding.cryptocurrency.text = currentItem.toString()
            binding.button.setOnClickListener {
                // pasar a ItemFragment el nombre de la criptomoneda como argumento
                var action = ListFragmentDirections.actionListFragmentToItemFragment(currentItem.toString())
                Navigation.findNavController(binding.root).navigate(action)
            }
        }
    }

    override fun getItemCount(): Int {
        return cryptocurrencyList.size
    }
}
