package com.uc3m.proyecto.api

import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    private val retrofit by lazy {

        val certificatePinner = CertificatePinner.Builder()
            .add("coinbase.com", "sha256//3nUp+rpWA0d0ufCVXAKqL+t4UcPfF6vx39HxbpqyMM=").build()

        val okHttpClient = OkHttpClient.Builder().certificatePinner(certificatePinner).build()

        Retrofit.Builder()
            .baseUrl("https://api.coinbase.com").addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    val cryptoAPI: CryptoAPI by lazy {
        retrofit.create(CryptoAPI::class.java)
    }
}
