package com.uc3m.proyecto.models

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Cryptocurrency::class], version = 1, exportSchema = false)
abstract class CryptocurrencyDatabase : RoomDatabase() {

    abstract fun cryptoDao(): CryptocurrencyDAO

    companion object {

        @Volatile
        private var INSTANCE: CryptocurrencyDatabase? = null

        fun getDatabase(context: Context): CryptocurrencyDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        CryptocurrencyDatabase::class.java,
                        "crypto_database"
                    ).fallbackToDestructiveMigration().build()
                }
                return instance
            }
        }
    }
}
