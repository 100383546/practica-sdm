package com.uc3m.proyecto.views

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.google.firebase.auth.FirebaseAuth
import com.uc3m.proyecto.R
import com.uc3m.proyecto.databinding.FragmentItemBinding
import com.uc3m.proyecto.models.Cryptocurrency
import com.uc3m.proyecto.models.Repository
import com.uc3m.proyecto.viewModels.CryptocurrencyViewModel
import com.uc3m.proyecto.viewModels.MainViewModel
import com.uc3m.proyecto.viewModels.MainViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ItemFragment : Fragment() {
    private lateinit var binding: FragmentItemBinding
    private lateinit var cryptoViewModel: CryptocurrencyViewModel
    private lateinit var auth: FirebaseAuth

    val args: ItemFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentItemBinding.inflate(inflater, container, false)
        val view = binding.root

        val text = args.text // guardar el nombre de la criptomoneda recibido como argumento

        binding.name.text = text // mostrar el nombre de la criptomoneda

        val repository = Repository()
        val viewModelFactory = MainViewModelFactory(repository)
        val viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)

        // obtener el precio de la criptomoneda y mostrar logo
        when (text) {
            "Bitcoin" -> {
                viewModel.getBitcoin()
                binding.imageView.setImageResource(R.drawable.bitcoin)
            }
            "Litecoin" -> {
                viewModel.getLitecoin()
                binding.imageView.setImageResource(R.drawable.litecoin)
            }
            "Ethereum" -> {
                viewModel.getEthereum()
                binding.imageView.setImageResource(R.drawable.ethereum_1)
            }
            "Stellar Lumens" -> {
                viewModel.getLumens()
                binding.imageView.setImageResource(R.drawable.stellar)
            }
            "Dash" -> {
                viewModel.getDash()
                binding.imageView.setImageResource(R.drawable.dash_3)
            }
        }

        viewModel.myResponse.observe(
            viewLifecycleOwner,
            Observer { response ->
                if (response.isSuccessful) {
                    val price = response.body()?.rates?.price?.eur
                    val result = String.format("%.2f", price) + " €"
                    binding.price.text = result // mostrar el precio de la criptomoneda
                } else {
                    Log.d("Response", response.errorBody().toString())
                }
            }
        )

        cryptoViewModel = ViewModelProvider(this).get(CryptocurrencyViewModel::class.java)

        lifecycleScope.launch {
            // obtener email del usuario
            auth = FirebaseAuth.getInstance()
            val currentUserEmail = auth.currentUser?.email.toString()

            val encryptedAmount = readAmount(currentUserEmail, text)
            if (encryptedAmount != null) {
                val encodedIV = readIv(currentUserEmail, text)
                val iv: ByteArray = Base64.decode(encodedIV, Base64.DEFAULT)
                val text: ByteArray = Base64.decode(encryptedAmount, Base64.DEFAULT)

                Log.d("amount", "onCreateView: $encryptedAmount")
                val amount = cryptoViewModel.decryptData(iv, text) // desencriptar dato
                binding.editTextNumberDecimal.setText(amount) // mostrar cantidad de criptomonedas

                val name = binding.name.text.toString()
                viewModel.myResponse.observe(
                    viewLifecycleOwner,
                    Observer { response ->
                        if (response.isSuccessful) {
                            val price = response.body()?.rates?.price?.eur
                            val result = String.format("%.2f", price)
                            val mult = amount.toFloat() * result.toFloat()
                            val res = "$amount $name is equivalent to $mult €" // mostrar cantidad en euros
                            binding.textView.text = res
                        } else {
                            Log.d("Response", response.errorBody().toString())
                        }
                    }
                )
            }
        }

        binding.button2.setOnClickListener {
            val amount = binding.editTextNumberDecimal.text.toString()
            if (amount.isNotEmpty()) {
                val name = binding.name.text.toString()
                viewModel.myResponse.observe(
                    viewLifecycleOwner,
                    Observer { response ->
                        if (response.isSuccessful) {
                            val price = response.body()?.rates?.price?.eur
                            val result = String.format("%.2f", price)
                            val mult = amount.toFloat() * result.toFloat()
                            val res = "$amount $name is equivalent to $mult €"
                            binding.textView.text = res
                        } else {
                            Log.d("Response", response.errorBody().toString())
                        }
                    }
                )
                updateDataToDatabase() // guardar cantidad de criptomonedas en la base de datos
            } else Toast.makeText(requireContext(), "It can´t be empty", Toast.LENGTH_LONG).show()
        }

        binding.button3.setOnClickListener { // redireccionar a coinbase

            val mContext = container?.context
            var intent = Intent()
            when (text) {
                "Bitcoin" -> {
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.coinbase.com/es/price/bitcoin"))
                }
                "Litecoin" -> {
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.coinbase.com/es/price/litecoin"))
                }
                "Ethereum" -> {
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.coinbase.com/es/price/ethereum"))
                }
                "Stellar Lumens" -> {
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.coinbase.com/es/price/stellar"))
                }
                "Dash" -> {
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.coinbase.com/es/price/dash"))
                }
            }

            mContext?.startActivity(intent)
        }

        return view
    }

    private fun updateDataToDatabase() {
        // obtener email del usuario
        auth = FirebaseAuth.getInstance()
        val currentUserEmail = auth.currentUser?.email

        val email = currentUserEmail.toString()
        val name = binding.name.text.toString()
        val amount = binding.editTextNumberDecimal.text.toString().toFloat()
        val pair = cryptoViewModel.encryptData(amount.toString()) // encriptar dato
        val encodedIV: String = Base64.encodeToString(pair.first, Base64.DEFAULT)
        val encodedText: String = Base64.encodeToString(pair.second, Base64.DEFAULT)

        val cryptocurrency = Cryptocurrency(email, name, encodedIV, encodedText)
        cryptoViewModel.insertCrypto(cryptocurrency)
        cryptoViewModel.updateCrypto(cryptocurrency)
        Toast.makeText(requireContext(), "Amount updated", Toast.LENGTH_LONG).show()
    }

    private suspend fun readAmount(email: String, name: String): String {
        val amount = withContext(Dispatchers.IO) {
            cryptoViewModel.readAmount(email, name)
        }
        return amount
    }

    private suspend fun readIv(email: String, name: String): String {
        val iv = withContext(Dispatchers.IO) {
            cryptoViewModel.readIv(email, name)
        }
        return iv
    }
}
