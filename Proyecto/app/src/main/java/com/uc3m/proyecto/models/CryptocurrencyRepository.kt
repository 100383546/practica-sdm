package com.uc3m.proyecto.models

class CryptocurrencyRepository(private val cryptoDao: CryptocurrencyDAO) {

    fun readAmount(email: String, name: String): String {
        return cryptoDao.readAmount(email, name)
    }

    fun readIv(email: String, name: String): String {
        return cryptoDao.readIv(email, name)
    }

    suspend fun updateCrypto(cryptocurrency: Cryptocurrency) {
        cryptoDao.updateCrypto(cryptocurrency)
    }

    suspend fun insertCrypto(cryptocurrency: Cryptocurrency) {
        cryptoDao.insertCrypto(cryptocurrency)
    }
}
